# cnl

A LISP that transpiles to Go (__**C**loud **N**ative **L**ISP__)
extension: .cnl

![cnl]("cnl.png")

## Problems

- How cnl's **namespaces** map to golang's packages?
- How to import from golang's stdlib (and third party)?
- What types are supported?
  https://learnyousomeerlang.com/types-or-lack-thereof
  http://homepages.inf.ed.ac.uk/wadler/papers/erlang/erlang.pdf
- How to generate golang AST?
- How to compile AST (static)?
- Grammar
- Syntax spec + Something like EDN spec
- CNL Parser (gocc?)
- Automatic documentation
- How to deal with **nil** values? http://www.rntz.net/post/2014-07-02-option-in-dynlangs.html
- Error Handling (erlang?, elixir?) https://learnyousomeerlang.com/errors-and-exceptions
- How to support macros?
- Immutable by default, Optional mutation
- STM?
- Persistent Data Structures
- CRI, CNI, OpenMetrics, OpenTracing?
- Microservices utils
- DevOps utils
- Really small core
- Unix philosophy
- ADTs? http://programming-puzzler.blogspot.com/2009/04/adts-in-clojure.html
- Static or dynamic typing? https://lispcast.com/clojure-and-types/
```
(defn handler [w :http/ResponseWriter r :http/Request] [:error]
  (http/write-response {:name "hehe" :id 2324} 200))
```
- enums? (defenum)
